<?php

/**
 * @file
 * Page callbacks include file for the fitness activity entity.
 */
/**
 * fitness_activity list page callback.
 */

 
function fitness_activity_list_page() {
  $elements = array();

  // entity_load() called with one argument loads all entities of the specified type.
  global $user;
  $user_id = user_load($user->uid);
  $query = db_query('SELECT * FROM {fitness_activity} f WHERE f.uid = :uid',array(':uid' => $user_id->uid ));
  $fitness_activity = $query->fetchAll();
  
  
//  $fitness_activitys = entity_load('fitness_activity', array($ids = 1));

  $header = array(t('Device type'), t('Calories burnt'), t('Activity Type'), t('Date'), t('Steps'),t('Duration'),t('Distance'));

  if(user_access('administer fitness activity')) {
    $header[] = t('Operation');
  }

  // Sort the fitness_activity by date, since entity_load() returns them unsorted.
  uasort($fitness_activity, create_function('$a,$b', 'return strcmp($a->date, $b->date);'));

  $rows = array();	
  
  
  foreach($fitness_activity as $eviga_fitness_activity_id => $fitness_activity) {
    $row = array();
    $row[] = check_plain($fitness_activity->device_type);
    $row[] = check_plain($fitness_activity->calories);
    $term = taxonomy_term_load($fitness_activity->activity_type);    
    $row[] = check_plain($term->name);
    $row[] = $fitness_activity->date;
    $row[] = check_plain($fitness_activity->steps);
    $row[] = check_plain($fitness_activity->duration);
    $row[] = check_plain($fitness_activity->distance);
    
    

    if(user_access('administer fitness activity')) {
      $act = l(t('edit'), "fitness-activity/". $fitness_activity->eviga_fitness_activity_id ."/edit", array('query' => drupal_get_destination()));
      $act .= ', ';
      $act .= l(t('delete'), "fitness-activity/". $fitness_activity->eviga_fitness_activity_id ."/delete", array('query' => drupal_get_destination()));
      $act .= ',';
   	$act .= l(t('View'), "fitness-activity/" . $fitness_activity->eviga_fitness_activity_id . "/view", array('attributes' => array('id' => 'bt-view-link' ,'class' => 'view-link')),array('query' => drupal_get_destination()));
		
      $row[] = $act;
    }
 
    $rows[] = $row;
  }


  $elements[''] = array(
    '#theme' => 'table',
    '#title' => t('Fitness Activitys'),
    '#header' => $header,
    '#rows' => $rows,
  );
  return $elements;
}

/**
 * fitness_activity add page callback.
 */
function fitness_activity_add() {
  // Invoke the create() method we added to the controller to get a new entity with defaults.
  $fitness_activity = entity_get_controller('fitness_activity')->create();

  return drupal_get_form('fitness_activity_form', $fitness_activity);
}

/**
 * fitness_activity add/edit form callback.
 */
function fitness_activity_form($form, &$form_state, $fitness_activity) {
  $form['#fitness_activity'] = $fitness_activity;

  $date = date_parse($fitness_activity->date);
  $form['date'] = array(
    '#type' => 'date',
    '#title' => t('Activity date'),
    '#default_value' => $date,
    '#required' => TRUE,
    
  );
  $form['device_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Device type'),
    '#default_value' => check_plain($fitness_activity->device_type),
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Enter Fitness device name you use.'),
   
  );
  $form['calories'] = array(
    '#type' => 'textfield',
    '#title' => t('Calories burnt'),
    '#default_value' => check_plain($fitness_activity->calories),
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Enter Calories you have burned.'),
  );
  $form['steps'] = array(
	'#type' => 'textfield',
	'#title' => t('Steps'),
	'#default_value' => check_plain($fitness_activity->steps),
	'#size' => 50,
	'#maxlength' => 255,
	'#required' => TRUE,
	'#description'=> t('Enter number of steps.'),
  );
	$form['distance'] = array(
	'#type' => 'textfield',
	'#title' => t('Distance'),
	'#default_value' => check_plain($fitness_activity->distance),
	'#size' => 50,
	'#maxlength' => 255,
	'#required' => TRUE,
	'#description' => t('Enter Distance you covered. It should be in Meters.'),
  );
   $form['duration'] = array(
	'#type' => 'textfield',
	'#title' => t('Duration'),
	'#default_value' => check_plain($fitness_activity->duration),
	'#size' => 50,
	'#maxlength' => 255,
	'#required' => TRUE,
	'#description' => t('Enter Duration you took to complete you activity. It should be in minute.'),
  );
  
   $v = taxonomy_vocabulary_machine_name_load('activity_type');
  $r = taxonomy_get_tree($v->vid);
  $options = array();
  foreach ($r as $term) {
    $options[$term->tid] = $term->name;
  }
  
  
  	$form['activity_type'] =array(
  		'#type' => 'select',
  		'#title' => t('Activity Type'),
  		'#options' => $options,
  		'#default_value' => check_plain($fitness_activity->activity_type),
    	'#size' => 10,		'#required' => TRUE,
  		'#description' => t('Please choose the activity type'),
  	);
  	global $user;
$user_fields = user_load($user->uid);

  $form['uid'] = array(
	'#type' => 'hidden',
	'#title' => t('uid'),
	'#default_value' => $user_fields->uid,
	);
    
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if(!empty($fitness_activity->eviga_fitness_activity_id)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#access' => user_access('administer fitness activity'),
      '#submit' => array('fitness_activity_form_delete_submit'),
    );
  }

  return $form;
}

/**
 * fitness_activity add/edit form submit callback.
 */
function fitness_activity_form_submit($form, &$form_state) {
  $fitness_activity = $form['#fitness_activity'];

  $date = $form_state['values']['date'];

  $fitness_activity->date = sprintf('%4d-%02d-%02d', $date['year'], $date['month'], $date['day']);
  $fitness_activity->device_type = $form_state['values']['device_type'];
  $fitness_activity->calories = $form_state['values']['calories'];
  $fitness_activity->activity_type = $form_state['values']['activity_type'];
  $fitness_activity->steps = $form_state['values']['steps'];
  $fitness_activity->distance = $form_state['values']['distance'];
  $fitness_activity->duration = $form_state['values']['duration'];
  $fitness_activity->uid = $form_state['values']['uid'];
  

  // Invoke the save() method that we added to the controller.
  entity_get_controller('fitness_activity')->save($fitness_activity);
  drupal_set_message(t('Fitness Activity saved.'));
  $form_state['redirect'] = 'fitness-activity/list';
}

/**
 * fitness_activity delete submit callback.
 *
 * The delete form logic was adapted from
 * Drupal 7 Module Development, by Butcher, et al. (Packt, 2010)
 */
function fitness_activity_form_delete_submit($form, &$form_state) {
  $destination = array();
  if(isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $fitness_activity = $form['#fitness_activity'];
  $form_state['redirect'] = array(
    'fitness-activity/' . $fitness_activity->eviga_fitness_activity_id . '/delete',
    array('query' => $destination),
  );
}

/**
 * fitness_activity delete confirm form callback.
 */
function fitness_activity_delete_confirm($form, &$form_state, $fitness_activity) {
  $form['#fitness_activity'] = $fitness_activity;
  $form['eviga_fitness_activity_id'] = array(
    '#type' => 'value',
    '#value' => $fitness_activity->eviga_fitness_activity_id,
  );
  return confirm_form($form,
    t('Are you sure you want to delete @fitness_activity?', array('@fitness_activity' => $fitness_activity->device_type)),
    'fitness-activity',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * fitness_activity delete confirm form submit callback.
 */
function fitness_activity_delete_confirm_submit($form, &$form_state) {
  if($form_state['values']['confirm']) {
    $fitness_activity = fitness_activity_load($form_state['values']['eviga_fitness_activity_id']);

    // Invoke the delete() method that we added to the controller.
    entity_get_controller('fitness_activity')->delete(array($fitness_activity->eviga_fitness_activity_id));
    drupal_set_message(t('Fitness activity "@fitness_activity" has been deleted.', array('@fitness_activity' => $fitness_activity->device_type)));
  }
  if(isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  else {
    $destination = array('destination' => 'fitness_activity');
  }
  $form_state['redirect'] = $destination['destination'];
}



