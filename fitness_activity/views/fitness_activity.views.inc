<?php



/**
* Implementing hook_views_data()
*/
function fitness_activity_views_data(){

$data['fitness_activity']['table']['group'] = t('Fitness Activity');

  
$data['fitness_activity']['eviga_fitness_activity_id'] = array(
     'title' => t('Fitness Activity Id'), 
    'help' => t('Id of your fitness activity'), 
    'field' => array(
	   'handler' => 'views_handler_field', 
      'click sortable' => TRUE,    
     ),
     
  );



$data['fitness_activity']['device_type'] = array(
     'title' => t('Device Type'), 
    'help' => t('Device type of your fitness activity'), 
    'field' => array(
	   'handler' => 'views_handler_field', 
      'click sortable' => TRUE,    
     ),
     
  );

  
$data['fitness_activity']['calories'] = array(
     'title' => t('Calories'), 
    'help' => t('calories burned'), 
    'field' => array(
	   'handler' => 'views_handler_field', 
      'click sortable' => TRUE,    
     ),
		'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),      
     
  );

  
$data['fitness_activity']['steps'] = array(
     'title' => t('Steps'), 
    'help' => t('Steps taken'), 
    'field' => array(
	   'handler' => 'views_handler_field', 
      'click sortable' => TRUE,    
     ),
     
  );
  
    
$data['fitness_activity']['distance'] = array(
     'title' => t('Distance'), 
    'help' => t('Distance covered'), 
    'field' => array(
	   'handler' => 'views_handler_field', 
      'click sortable' => TRUE,    
     ),
     
  );
    
$data['fitness_activity']['duration'] = array(
     'title' => t('Druration'), 
    'help' => t('Duration taken'), 
    'field' => array(
	   'handler' => 'views_handler_field', 
      'click sortable' => TRUE,    
     ),
     
  );
  $data['fitness_activity']['date'] = array(
     'title' => t('Activity Date'), 
    'help' => t('Activity Date'), 
    'field' => array(
	   'handler' => 'views_handler_field', 
      'click sortable' => TRUE,    
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
     'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),   
  );

return $data;
}
