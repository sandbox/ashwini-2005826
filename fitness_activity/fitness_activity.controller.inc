<?php

/**
 * @file
 * Controller class definition file for the "fitness_activity" entity.
 */

/**
 * fitness_activity Controller
 */
class fitness_activityController extends DrupalDefaultEntityController {
  public function __construct($entityType) {
    parent::__construct('fitness_activity');
  }

  // The default controller doesn't provide a create() method, so add one.
  // This is a useful place to assign defaults to the properties.
  public function create() {
    $fitness_activity = new StdClass();
    $fitness_activity->date = format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i:s');
    $fitness_activity->device_type = '';
    $fitness_activity->calories = '';
    $fitness_activity->steps = '';
    $fitness_activity->distance = '';
    $fitness_activity->duration = '';
    $fitness_activity->activity_type = '';
    return $fitness_activity;
  }
/**
   * Overriding the buldContent function to add entity specific fields
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $content = parent::buildContent($entity, $view_mode, $langcode, $content);
    $content['device_type'] =  array(
      '#markup' => theme('device_type', array('device_type' => check_plain($entity->data['device_type']), 'fitness_activity' => $entity)),
    );

    return $content;
  }
  // The default controller doesn't provide a save() method, so add one.
  public function save($fitness_activity) {
    if(empty($fitness_activity->eviga_fitness_activity_id)) {
      drupal_write_record('fitness_activity', $fitness_activity);

      // Make sure to invoke the insert hook.
      module_invoke_all('entity_insert', $fitness_activity, 'fitness_activity');
    }
    else {
      drupal_write_record('fitness_activity', $fitness_activity, 'eviga_fitness_activity_id');

      // Make sure to invoke the update hook.
      module_invoke_all('entity_update', $fitness_activity, 'fitness_activity');
    }

    return $fitness_activity;
  }

  // The default controller doesn't provide a delete() method, so add one.
  public function delete($eviga_fitness_activity_ids) {
    if(empty($eviga_fitness_activity_ids)) {
      return FALSE;
    }

    $fitness_activitys = entity_load('fitness_activity', $eviga_fitness_activity_ids);
    db_delete('fitness_activity')
      ->condition('eviga_fitness_activity_id', $eviga_fitness_activity_ids, 'IN')
      ->execute();

    // Make sure to invoke the delete hook for each fitness_activity.
    foreach($fitness_activitys as $fitness_activity) {
      module_invoke_all('entity_delete', $fitness_activity, 'fitness_activity');
    }

    // It's necessary to clear the caches when an entity is deleted.
    cache_clear_all();
    $this->resetCache();
    return TRUE;
  }
}
