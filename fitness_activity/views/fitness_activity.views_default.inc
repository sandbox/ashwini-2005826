<?php









/**
* Implementing hook_views_default_views() 
*/

function fitness_activity_views_default_views(){

	$views = array();
	$view = new view;
	$view->name = 'fitness_activity';
	$view->description = 'Fitness activity views for listing all activities';
	$view->tag = 'fitness activity';
	$view->base_table = 'fitness_activity';
	$view->human_name = 'fitness_activity';
	$view->machine_name = 'fitness_activity';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /*Edit this to true to make default view disable initially*/
	
 /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Fitness activity';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer fitness activity';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'device_type' => 'device_type',
    'eviga_fitness_activity_id' => 'eviga_fitness_activity_id',
  );
	 
	 $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'device_type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'eviga_fitness_activity_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  
  
  
  
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'frontpage';


$views[$view->name] = $view;

return $views;



}
